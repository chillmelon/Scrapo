import bs4
import urllib.parse as urlparse
import time
import cfscrape
def printPage(url:str):
    scraper = cfscrape.create_scraper()
    html = scraper.get(url).content
    # 解析網頁原始碼
    source = bs4.BeautifulSoup(html, "html.parser")
    try:
        # 擷取所有內容
        content = '' #內容
        page = source.find("div", {'class':'detail_content'})
        paragraphs = page.find_all("p")
        if paragraphs == None:
            pass
        else:
            # 收集所有內人(p)
            for p in paragraphs:
                # 過濾包含圖片來源的字串
                if '圖片來源' in p.text:
                    continue
                content += p.text.strip()
            content += '", "'
            return content
    except:
        print('完全沒有畫面')
        return content

def search(page, count, text):
    url = "https://www.buzzhand.com/health.html?page=" + str(page)
    scraper = cfscrape.create_scraper()
    my_header = {
        'useragent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'
    }
    # GET request
    html = scraper.get(url, headers=my_header).content
    # 解析頁面原始碼
    root = bs4.BeautifulSoup(html, "html.parser")
    # 擷取所有標題
    titles = root.find_all("h3", {"class":"title"})
    # 從標題裡抓出url,並剔除廣告標題(no a)
    for title in titles:
        if title.find('a') == None:
            continue
        else:
            href = title.find('a').get('href')
            news_title = title.text
            # 拿到網址後把它補全再用printPage抓內容
            newLink = 'https://www.buzzhand.com' + href
            text += printPage(newLink)
            count += 1
    print('======'+str(page)+'======')
    if root.find('li', {'class':'next hidden'}):
        return page, count, text
    else:
        return search(page+1, count, text)
            
def constantScrape(start):
    page, count, content = search(start, 0, '')
    # 把爬到資料寫入檔案
    f = open('health.json', 'w', encoding = 'utf8')
    f.write('"'+content)
    f.close()   
    print('完成！！')
    print('共'+str(page-start+1)+'頁',str(count)+'篇')

def main():
    startTime=time.time()
    start = int(input())
    print(str(start) + '開始')
    constantScrape(start)
    endTime=time.time()
    print('耗時'+str(endTime - startTime)+'seconds')

if __name__ == '__main__':
    main()


